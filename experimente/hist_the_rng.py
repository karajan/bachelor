#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

# laden von Daten, falls gewünscht

data = np.loadtxt("build/erg.txt", float)
x1 = data[:, 0]
x2 = data[:, 1]
x3 = data[:, 2]


# Funktion fitten
# fitfunc = lambda n, a, b, x: a * n ** x + b


# Werte und Fit plotten
# plt.hist(ns, 50)
# x = np.linspace(0, 10, 1000)
# plt.plot(x, 6*x**2, "r")

from mpl_toolkits.mplot3d import Axes3D  # 3D-Plots

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x1, x2, x3)

# plt.scatter(x1, x2)

plt.show()
