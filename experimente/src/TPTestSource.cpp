/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#include "./TPTestSource.h"
#include "./TPEasyMuon.h"


TPTestSource::TPTestSource() {
  TPEasyMuon::initPropagator();
}

TPTestSource::TPTestSource(vec_t detectorDimensions, double energyExponent,
                           double energyMin, double energyMax) :
  TInIce(detectorDimensions, energyExponent, energyMin, energyMax) {
  TPTestSource::TPTestSource();
}


TEvent &TPTestSource::GetNewEvent() {
  double radius = padif::absolute(detectorDimensions_) * 0.5;  // sphere size
  radius = 4 * radius;  // make the sphere a little bigger than the detector

  // produce a starting point to feed to the GetNewTrack-Method
  // and move the center of the sphere to the center of the detector
  vec_t newTrack(5), start(3);
  vec_t onSphere = padif::getUniSphere();
  for (int i = 0; i < 3; i++) {
    start.at(i) = onSphere.at(i) * radius + detectorDimensions_.at(i) / 2.0;
  }

  // this method is guaranteed to produce a good track so no checking
  newTrack = GetNewTrack(start);
  vec_t position(3);
  double azimuthalAngle = newTrack.at(3);
  double polarAngle = newTrack.at(4);
  for (uint j = 0; j < 3; j++) {
    position.at(j) = newTrack.at(j);
  }

  double energy = GetNewEnergy();

  TEvent *event = new TPEasyMuon();
  // fill its values with just generated values
  event->SetEnergy(energy);
  event->SetAzimuthalAngle(azimuthalAngle);
  event->SetPolarAngle(polarAngle);
  event->SetPosition(position);

  return *event;
}
