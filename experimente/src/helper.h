/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#ifndef HELPER_H
#define HELPER_H


#include <boost/random/mersenne_twister.hpp>
#include <vector>
#include <string>


typedef std::vector<double> vec_t;

namespace padif {

const double speedOfLight = 3e8;  // in m / s


/**
* @brief degToRad calculates the radiant of a given degree
* @param degree the value to be transformened into a radiant
* @return the radiant to a given degree
*/
double degToRad(double degree);

/**
* @brief distance calculates the distance between two 3D-points
* @param pnt1 is the first point
* @param pnt2 is the second point
* @return the distance
*/
double distance(const vec_t &pnt1, const vec_t &pnt2);

/**
* @brief absolute calculates the distance between a 3D-point and the
* origin (0,0,0) aka calculates the absolute of a vector
* @param point is the point
* @return the absolute
*/
double absolute(const vec_t &point);

/**
 * @brief dumpVector creates a well readable output of the given vector
 * @param phrase awaits a string describing the use of this vector
 * @param position awaits a 3 dimensional vector
 */
void dumpVector(std::string phrase, const vec_t &vector);

/**
 * @brief sphericalToCartesian turns a spherical coordinate into a cartesian
 * coordinate
 * @param r is the distance from the point to the origin
 * @param azimuthal is the azimuthal angle of the point
 * @param polar is the polar angle of the point
 * @return a vector with the cartesian coordinates
 */
vec_t sphericalToCartesian(double r, double azimuthal, double polar);

/**
 * @brief innerProduct calculates the inner product of the euclidean
 * space
 * @param aFactor is the first factor
 * @param bFactor is the second factor
 * @return the inner product of two vectors in the euclidean space
 */
double innerProduct(const vec_t &aFactor, const vec_t &bFactor);


/**
 * this is the static rng to be used to create all random numbers necessary
 * for this program
 */
static boost::mt19937 rng_;

/**
 * @brief setSeed sets the seed for the rng to the value given
 * @param seed is the seed
 */
void setSeed(int seed);

/**
 * @brief getUni calculates a random number between 0.0 and 1.0 from
 * a uniform distribution 
 * @return a random number between 0.0 and 1.0
 */
double getUni();

/**
 * @brief getUni calculates a random number between the given values min
 * and max from a uniform distribution 
 * @param min the minimum value
 * @param max the maximum value
 * @return a random number between min and max
 */
double getUni(double min, double max);

/**
 * @brief getUniSphere calculates a 3D vector with the coordinates of a point
 * on a sphere with radius 1 uniformly distributed
 * @return 3D vector with the coordinates of a point on a sphere
 */
vec_t getUniSphere();

/**
 * @brief getUniSphere calculates a n-D vector with the coordinates of a point
 * on a sphere with radius 1 uniformly distributed
 * @param dimesions is the number of dimesions the sphere should be in
 * @return n-D vector with the coordinates of a point on a sphere
 */
vec_t getUniSphere(uint dimensions);

/**
 * @brief getCubed calculates a random number between 0.0 and 1.0 from
 * a cubic distribution; if you want evenly distributed points inside
 * a sphere you will need this
 * @return a random number between 0.0 and 1.0
 */
double getCubed();
}


#endif  // HELPER_H
