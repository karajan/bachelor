/**
 *  Author: Philipp Schlunder
 *  Licence: GPL v3
 */

#include <fstream>
#include <iostream>

#include "./TDetector.h"

TDetector::TDetector() : edgeLength_(0) {
  sensors_.resize(0);
  groups_.resize(0);
  // put all sensors in one group since all are created with id = 1
  groups_.push_back(sensors_);
}

TDetector::TDetector(double edgeLength, int numberOfSensors)
  : edgeLength_(edgeLength) {
  sensors_.resize(0);
  groups_.resize(0);
  PlaceSensors(numberOfSensors, 1.0);
  // save sensor position into detector_config.txt
  WriteToFile();
  // put all sensors in one group since all are created with id = 1
  groups_.push_back(sensors_);
}

TDetector::TDetector(double edgeLength, int numberOfSensors,
                     double permeability)
  : edgeLength_(edgeLength) {
  PlaceSensors(numberOfSensors, permeability);
  // save sensor position into detector_config.txt
  WriteToFile();
  // put all sensors in one group since all are created with id = 1
  groups_.push_back(sensors_);
}

TDetector::TDetector(std::string filename) : edgeLength_(0) {
  sensors_.resize(0);
  double x, y, z, permeability;
  unsigned int group;
  // open file with given filename for reading
  std::ifstream file(filename.c_str());
  // read the first line with commentary
  std::string commentLine;
  std::getline(file, commentLine);
  // read all values and put them together into a position
  while (file.good()) {
    file >> x >> y >> z >> permeability >> group;
    vec_t tempPosition(3);
    tempPosition.at(0) = x;
    tempPosition.at(1) = y;
    tempPosition.at(2) = z;
    // create a new sensor out of the given values and add it to the detector
    AddSensor(new TSensor(tempPosition, permeability, group));
  }
  // sort sensors into groups
  GroupSensors();
  // set edgeLength_ to the highest used coordinate
  SetEdgeLength();
}

void TDetector::AddSensor(TSensor *newSensor) {
  // check whether the position of the newSensor is already taken
  if (!PositionTaken(newSensor->GetPosition())) {
    // if not, add the new sensor
    sensors_.push_back(newSensor);
  }
}

// TODO: mit Philipp reden und das richtig machen
void TDetector::Detect(const TEvent &event, bool add) {
  // measure for each sensor
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    TSensor *sensor = sensors_.at(i);

    vec_t sensorPosition = sensor->GetPosition();
    vec_t perpFootTemp = event.GetPerpFoot(sensorPosition);

    // the event began behind the sensor
    if (perpFootTemp.at(3) < 0) {
      continue;
    }

    vec_t perpFoot(3);
    for (int i = 0; i < 3; i++) {
      perpFoot.at(i) = perpFootTemp.at(i);
    }

    // use energy the event has at its nearest position to the sensor
    double energy = event.GetEnergy(perpFoot);
    double trackLength = padif::distance(event.GetPosition(), perpFoot);
    double perpTime = trackLength / padif::speedOfLight;

    sensor->Detect(energy, perpTime, add, perpFoot);
  }
}

// void TDetector::Detect(const TEvent &event, bool add) {
//   double speedOfLight = 3e8;  // in m / s
//   // measure for each sensor
//   for (unsigned int i = 0; i < sensors_.size(); i++) {
//     TSensor *sensor = sensors_.at(i);

//     vec_t sensorPosition = sensor->GetPosition();
//     vec_t perpFootTemp = event.GetPerpFoot(sensorPosition);

//     // if the intensity isn't supposed to be added, set it 0
//     if (!add) {
//       sensor->SetIntensity(0);
//       sensor->SetArrivalTime(0);
//     }

//     // the event began behind the sensor
//     if (perpFootTemp.at(3) < 0) {
//       continue;
//     }

//     vec_t perpFoot(3);
//     for (int i = 0; i < 3; i++) {
//       perpFoot.at(i) = perpFootTemp.at(i);
//     }

//     // use energy the event has at its nearest position to the sensor
//     double energy = event.GetEnergy(perpFoot);
//     std::cout << energy << std::endl;
//     // trigger sensor based on energy dependent acceptance
//     // TODO: wäre es geschickter, das hier in den Sensor zu verschieben?
//     if (pow((1 - exp(- energy / 2.0)), 3) > padif::getUni()) {
//       double permeability = sensor->GetPermeability();
//       double sensorDistance = padif::distance(perpFoot, sensorPosition);
//       double trackLength = padif::distance(event.GetPosition(), perpFoot);

//       // arrival time is the time the event needs to reach the nearest
//       // position to the sensor + time the photons need from this position to
//       // the sensor
//       sensor->SetArrivalTime((sensorDistance + trackLength)
//                              / speedOfLight);
//       // calculate deposited energy and add it to the stored value
//       sensor->SetIntensity(sensor->GetIntensity()
//                            + energy * permeability
//                            / (1 + pow(sensorDistance, 2)));
//     }
//   }
// }


void TDetector::Dump() const {
  std::cout << "EdgeLength: " << edgeLength_ << std::endl;
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    std::cout << "Sensor " << i + 1 << ":" << std::endl;
    sensors_.at(i)->Dump();
  }
}

vec_t TDetector::GetCenterOfIntensity() const {
  // need to calculate the center for each coordinate
  vec_t center(3);

  // calculate center of intensity for each coordiante
  for (unsigned int pos = 0; pos < 3; pos++) {
    // sum of (with intensity) weighted positions
    double numerator = 0;
    // sum of weights (intensities)
    double denominator = 0;

    // calculate numerator and denominator
    for (unsigned int event = 0; event < sensors_.size(); event++) {
      double weight = sensors_.at(event)->GetIntensity();
      double position = sensors_.at(event)->GetPosition().at(pos);

      numerator += weight * position;
      denominator += weight;
    }
    // center of intensity = sum of weighted positions / sum of weights
    center.at(pos) = numerator / denominator;
  }

  return center;
}

vec_t TDetector::GetDetectorDimensions() const {
  vec_t detectorDimensions(3);
  double maxx = 0, maxy = 0, maxz = 0;
  std::vector<TSensor *> sensors = GetSensors();

  for (unsigned int i = 0; i < GetNumberOfSensors(); i++) {
    vec_t position = sensors.at(i)->GetPosition();
    if (position.at(0) > maxx) {
      maxx = position.at(0);
    }
    if (position.at(1) > maxy) {
      maxy = position.at(0);
    }
    if (position.at(2) > maxz) {
      maxz = position.at(0);
    }
  }
  detectorDimensions.at(0) = maxx;
  detectorDimensions.at(1) = maxy;
  detectorDimensions.at(2) = maxz;

  return detectorDimensions;
}

double TDetector::GetEdgeLength() const {
  return edgeLength_;
}

const std::vector<TSensor *> &TDetector::GetGroup(unsigned int id) const {
  if (id > 0) {
    // need to decrease the id because a vector starts with 0 and not with 1
    return groups_.at(id - 1);
  } else {
    std::cout << "Id has to exceed 0. Returning group with id 1." << std::endl;
    std::cout << "Keep in mind: The actual id is used, not its vector position."
              << std::endl;
    return groups_.at(0);
  }
}

unsigned int TDetector::GetNumberOfGroups() const {
  return groups_.size();
}

unsigned int TDetector::GetNumberOfSensors() const {
  return sensors_.size();
}

const std::vector<TSensor *> &TDetector::GetSensors() const {
  return sensors_;
}

void TDetector::GroupSensors() {
  unsigned int maxId = 0;

  // loop over sensors to find the max id
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    if (sensors_.at(i)->GetGroup() > maxId) {
      maxId = sensors_.at(i)->GetGroup();
    }
  }

  // set group_ size to max id
  groups_.resize(maxId);

  // insert dummy vector with dummy sensor into groups_ to initialize it
  // need to push_back vectors of sensors to groups_ thats why one needs a dummy
  for (unsigned int i = 0; i < groups_.size(); i++) {
    std::vector<TSensor *> dummy;
    dummy.push_back(new TSensor());
    groups_.at(i) = dummy;
  }

  // actual sorting process
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    unsigned int id = sensors_.at(i)->GetGroup();
    // sort sensor into group with the given id
    // keep in mind a vector starts with 0 but id's start with 1
    groups_.at(id - 1).push_back(sensors_.at(i));
  }

  // erase dummy vectors and sensors
  for (unsigned int i = 0; i < groups_.size(); i++) {
    delete groups_.at(i).at(0);
    groups_.at(i).erase(groups_.at(i).begin());
  }
}

void TDetector::PlaceSensors(int numberOfSensors, double permeability) {
  sensors_.resize(0);
  // test if edgeLength exceeds 0
  if (edgeLength_ >= 1) {
    int LoopLimit = 0;  // limit for sensor coordinates
    double cubeRoot = pow(numberOfSensors, 1. / 3.);
    LoopLimit = static_cast<int>(pow(numberOfSensors, 1. / 3.));
    // There are LoopLimit^3 positions for sensors available, if numberOfSensors
    // exceeds this amount it is necessary to use (LoopLimit+1)^3 positions.
    if ((cubeRoot - LoopLimit) > 0) LoopLimit++;
    vec_t tempPosition;  // holds generated sensor positions
    for (int i = 0; i < 3; i++) tempPosition.push_back(0);  // default values
    double SensorCounter = 0;  // counter for the amount of placed sensors
    // calculate distance for equidistant sensor placement
    double distance = (edgeLength_) / (LoopLimit + 1);
    // --- generate sensor positions ---
    for (int z = 1; z <= LoopLimit; z++) {
      for (int y = 1; y <= LoopLimit; y++) {
        for (int x = 1; x <= LoopLimit; x++) {
          // until enough sensors are created
          if (SensorCounter >= numberOfSensors) {
            break;
          }
          // each coordinate is dependend on the amount of existing sensors in
          // the given direction and the distance between each sensor
          tempPosition.at(0) = distance * x;
          tempPosition.at(1) = distance * y;
          tempPosition.at(2) = distance * z;
          vec_t tempVector;
          for (int i = 0; i < 3; i++) {
            tempVector.push_back(tempPosition.at(i));
          }
          // add the new sensor to the detector
          sensors_.push_back(new TSensor(tempVector, permeability, 1));
          SensorCounter++;
        }
      }
    }
  } else {
    std::cout << "The edge length of the detector has to exceed 0."
              << std::endl;
  }
}

bool TDetector::PositionTaken(vec_t testPosition) const {
  // initial assumption is that the position is not taken
  bool isTaken = false;

  // compare the coordinates of testPosition against all existing sensors
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    const vec_t position = sensors_.at(i)->GetPosition();
    if (testPosition.at(0) == position.at(0) &&
        testPosition.at(1) == position.at(1) &&
        testPosition.at(2) == position.at(2)) {
      sensors_.at(i)->Dump();
      // if there is a match, end the loop and change marker "isTaken" to true
      isTaken = true;
      // This might also be triggered if your detector_config.txt ends with an
      // empty line. If that's the case, just ignore the message.
      std::cout << i << " TDetector: The chosen position is already taken."
                << std::endl;
      break;
    }
  }
  return isTaken;
}

const TEvent &TDetector::ReconstructTrack() const {
  vec_t mean(3, 0);  // storage for x, y and z position of the means
  vec_t offset(2), gradient(2);  // calculated offsets and gradients
  std::vector<vec_t > positions(3);  // positions of activated sensors

  // sum up and store coordinates of the activated sensors
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    TSensor *sensor = sensors_.at(i);
    // check if sensors has been activated
    if (sensor->GetIntensity() > 0) {
      // for each coordinate
      for (unsigned int j = 0; j < 3; j++) {
        double pos = sensor->GetPosition().at(j);
        // add it to the sum
        mean.at(j) += pos;
        // store position
        positions.at(j).push_back(pos);
      }
    }
  }

  // get number of activated sensors
  double activatedSensors = positions.at(0).size();
  // convert sum of coordinates to mean
  for (unsigned int i = 0; i < 3; i++) {
    // one does not simply divide by zero
    if (activatedSensors > 0) {
      mean.at(i) /= activatedSensors;
    }
  }

  // calculate offset and gradient of a 2 dimension line
  // first in the (x, y)-plane, then in the (y, z)-plane
  for (unsigned int i = 0; i < 2; i++) {
    double numerator = 0, denominator = 0;

    /**
     * using the analytic solution for a 2 dimensional linear regression
     * gradient = sum_i^n ((x_i - mean(x)) * (y_i - mean(y)))
     *          / sum_i^n ((x_i - mean(x)) ^ 2)
     * offset = mean(y) - gradient * mean(x)
     */
    for (unsigned int j = 0; j < activatedSensors; j++) {
      // calculate the numerator and denominator separatly
      numerator += (positions.at(0 + i).at(j) - mean.at(0 + i))
                   * (positions.at(1 + i).at(j) - mean.at(1 + i));
      denominator += pow((positions.at(0 + i).at(j) - mean.at(0 + i)), 2.);
    }
    // and execute the division
    gradient.at(i) = numerator / denominator;
    offset.at(i) = mean.at(1 + i) - gradient.at(i) * mean.at(0 + i);
  }

  /**
   * preparing a position that might be part of the track
   * y(x) = offset[0] + x * gradient[0]
   * z(y) = offset[1] + y * gradient[1]
   * => z(x) = offset[1] + (offset[0] + x * gradient[0]) * gradient[1]
   * x = 0:
   * => y = offset[0]
   * => z = offset[1] + offset[0] * gradient[1]
   */
  vec_t trackPosition(3);
  trackPosition.at(0) = 0;
  trackPosition.at(1) = offset.at(0);
  trackPosition.at(2) = offset.at(0) * gradient.at(1) + offset.at(1);

  /**
   * Store track information as TEvent object, because it provides all
   * variables needed, besides the additional energy parameter. This provides
   * the huge advantage, that one can receive projections of positions
   * (eg. from sensors) to the reconstructed track via using
   * TEvent.GetPerpFoot().
   */
  double azimuthalAngle, polarAngle;
  azimuthalAngle = atan(gradient.at(0));
  polarAngle = (M_PI / 2) - atan(gradient.at(1));

  TEvent *reconstructedTrack = new TEvent(0,
                                          trackPosition,
                                          azimuthalAngle,
                                          polarAngle);

  return *reconstructedTrack;
}


void TDetector::ResetSensors() {
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    sensors_.at(i)->SetIntensity(0);
  }
}

void TDetector::SetEdgeLength() {
  double newEdgeLength = 0;
  for (unsigned int sensor = 0; sensor < sensors_.size(); sensor++) {
    for (int position = 0; position < 3; position++) {
      if (sensors_.at(sensor)->GetPosition().at(position) > newEdgeLength) {
        newEdgeLength = sensors_.at(sensor)->GetPosition().at(position);
      }
    }
  }
  edgeLength_ = newEdgeLength;
}

void TDetector::SetEdgeLength(double edgeLength) {
  if (edgeLength > 0) {
    edgeLength_ = edgeLength;
  } else {
    std::cout << "Please use a positive edge length." << std::endl;
    std::cout << "No new edge length set." << std::endl;
  }
}

void TDetector::WriteToFile() {
  std::ofstream file("../detector_config.txt");
  file << "#x\t y\t z\t permeability\t group" << std::endl;
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    file << sensors_.at(i)->GetPosition().at(0) << "\t"
         << sensors_.at(i)->GetPosition().at(1) << "\t"
         << sensors_.at(i)->GetPosition().at(2) << "\t"
         << sensors_.at(i)->GetPermeability() << "\t"
         << sensors_.at(i)->GetGroup() << std::endl;
  }
  file.close();
}

TDetector::~TDetector() {
  // Free the memory used by the sensors
  for (unsigned int i = 0; i < sensors_.size(); i++) {
    delete sensors_.at(i);
  }
}
