/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#include <string>
#include "./TPEasyMuon.h"


Propagator *TPEasyMuon::prop = 0;


TPEasyMuon::TPEasyMuon() { }

TPEasyMuon::TPEasyMuon(double energy, const vec_t &position,
                       double azimuthalAngle) :
  TEvent(energy, position, azimuthalAngle) {
  TPEasyMuon::TPEasyMuon();
}

TPEasyMuon::TPEasyMuon(double energy, const vec_t &position,
                       double azimuthalAngle, double polarAngle) :
  TEvent(energy, position, azimuthalAngle, polarAngle) {
  TPEasyMuon::TPEasyMuon();
}


double TPEasyMuon::GetEnergy(const vec_t &referencePosition) const {
  prop->GetParticle()->SetEnergy(energy_);
  double distance = padif::distance(position_, referencePosition);
  prop->Propagate(distance * 100);  // we use m, they use cm
  return prop->GetParticle()->GetEnergy();
}

void TPEasyMuon::initPropagator() {
  // Name, Dichtekorrektur
  Medium *medium = new Medium("ice", 0.832);
  // Mindestenergie für stochastische Verluste
  // (absolute Grenze in MeV, relative Grenze * Teilchenenergie)
  EnergyCutSettings *cut = new EnergyCutSettings(-1, -1);
  // Pfad wo deine Interpolationstabellen gespeichert werden.
  std::string PathToTables = "";
  // Teilchenstreuung an (true) oder aus (false)
  bool moliereScattering = false;
  // kontinuierliche Energieverluste werden verschmiert (true),
  // dann kommt es auf die reihenfolge der Sensoren an
  bool contRand = false;
  // exakte Zeit berechnen (true) oder Teilchen bewegt sich mit c (false)
  bool exactTime = true;

  prop = new Propagator(medium, cut, "mu", PathToTables,
                        moliereScattering, contRand, exactTime);
}


TPEasyMuon::~TPEasyMuon() { }
