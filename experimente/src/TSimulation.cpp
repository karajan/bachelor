/**
 *  Author: Philipp Schlunder, Jens Adam
 *  Licence: GPL v3
 */


#include <TStyle.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "./TEvent.h"
#include "./TDetector.h"
#include "./TInIce.h"
#include "./TSimulation.h"
#include "./TInIce.h"

namespace po = boost::program_options;


TSimulation::TSimulation() {
  // set global ROOT style
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(1);

  // create detector
  detectorDimensions_.resize(3);
  edgeLength_ = 1000;
  detectorDimensions_.at(0) = edgeLength_;
  detectorDimensions_.at(1) = edgeLength_;
  detectorDimensions_.at(2) = edgeLength_;
  numberOfSensors_ = 100;
  detector_ = new TDetector(edgeLength_, numberOfSensors_);
  std::cout << "Created detector with an edge length of " << edgeLength_
            << " meters and " << numberOfSensors_ << " sensors." << std::endl;

  // distributions:
  energyExponent_ = 2.7;
  energyMin_ = 1e1;
  energyMax_ = 1e5;
  azimuthalSpectrum_ = 2;
  polarSpectrum_ = 0;

  amountOfSamples_ = 1e4;
  measuredSamples_ = 0;

  eventId = 0;
  output_ = "result";
  position.assign(3, 0);

  // initalize ROOT TTree
  InitTTree();

  // set seed to static rng
  seed_ = 0;
  padif::setSeed(seed_);
}

TSimulation::TSimulation(std::string configFile) {
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(1);

  InitConfig();
  LoadConfig(configFile);

  eventId = 0;
  position.assign(3, 0);

  padif::setSeed(seed_);

  InitTTree();
}

void TSimulation::InitConfig() {
  // po::options_description desc_("parameters");
  desc_.add_options()
  ("settings.seed", po::value<int>(&seed_),
   "seed for the random number generators")
  ("settings.edgeLength", po::value<double>(&edgeLength_),
   "edge length of the detector")
  ("settings.numberOfSensors", po::value<int>(&numberOfSensors_),
   "number of sensors the detector should hold")
  ("settings.amountOfSamples", po::value<int>(&amountOfSamples_),
   "amount of samples that should be created")
  ("energy.exponent", po::value<double>(&energyExponent_),
   "negative value of exponent is used as exponent for energy distribution")
  ("energy.min", po::value<double>(&energyMin_),
   "minimum of the en ergy spectrum")
  ("energy.max", po::value<double>(&energyMax_),
   "maximum of the energy spectrum")
  ("origin.azimuthal", po::value<double>(&azimuthalSpectrum_),
   "factor of pi for azimuthal angle")
  ("origin.polar", po::value<double>(&polarSpectrum_),
   "factor of pi for polar angle")
  ("settings.output", po::value<std::string>(&output_), "output filename")
  ("settings.detector", po::value<std::string>(&detectorConfig),
   "config file for detector");
}

void TSimulation::LoadConfig(std::string configFile) {
  // --- reading settings ---
  // open config file
  std::ifstream config_file(configFile.c_str());
  // clear parameter storage before reading
  vm_ = po::variables_map();
  // read parameters from file into map
  po::store(po::parse_config_file(config_file, desc_), vm_);
  config_file.close();
  po::notify(vm_);

  // Winkel passend einschränken
  if ((azimuthalSpectrum_ < 0) || (azimuthalSpectrum_ > 2)) {
    azimuthalSpectrum_ = 2;
    std::cout << "Origin.azimuthal has to be in range [0,2]. "
              << "Set it back to 2." << std::endl;
  }
  if ((polarSpectrum_ <= 0) || (polarSpectrum_ > 1)) {
    polarSpectrum_ = 1;
    std::cout << "Origin.polar has to be in range [0,1]. "
              << "Set it back to 1." << std::endl;
  }

  // Detektor erstellen
  if (detectorConfig.size() != 0) {
    detector_ = new TDetector(detectorConfig);
    edgeLength_ = detector_->GetEdgeLength();
    numberOfSensors_ = detector_->GetNumberOfSensors();
    std::cout << "Created detector from " << detectorConfig << std::endl;
  } else if (edgeLength_ == 0 && numberOfSensors_ == 0) {
    edgeLength_ = 1000;
    detector_ = new TDetector(1000, 100);
    std::cout << "Created detector with an edge length of " << 1000
              << " meters and " << 100 << " sensors." << std::endl;
  } else {  // or just create a detector with equally distributed sensors
    // TODO: Das hier könnte schief gehen, fixen
    detector_ = new TDetector(edgeLength_, numberOfSensors_);
    std::cout << "Created detector with an edge length of " << edgeLength_
              << " meters and " << numberOfSensors_ << " sensors."
              << std::endl;
  }

  detectorDimensions_ = detector_->GetDetectorDimensions();
}

void TSimulation::InitTTree() {
  output_ += ".root";
  rootFile = new TFile(output_.c_str(), "RECREATE");
  data = new TTree("run", "raw data of an event");

  // assign placeholders with branch attributes
  data->Branch("1stSensor_XPosition", &firstSensorX,
               "1stSensor_XPosition/D");
  data->Branch("1stSensor_YPosition", &firstSensorY,
               "1stSensor_YPosition/D");
  data->Branch("1stSensor_ZPosition", &firstSensorZ,
               "1stSensor_ZPosition/D");
  data->Branch("CoI_XPosition", &centerOfIntensityX, "CoI_XPosition/D");
  data->Branch("CoI_YPosition", &centerOfIntensityY, "CoI_YPosition/D");
  data->Branch("CoI_ZPosition", &centerOfIntensityZ, "CoI_ZPosition/D");
  data->Branch("Event_ID", &eventId, "Event_ID/Is");
  data->Branch("Event_Seed", &seed_, "Event_Seed/I");
  data->Branch("Effective_Track_Length", &effectiveTrackLength,
               "Effective_Track_Length/D");
  data->Branch("MC_Energy0", &energy, "MC_Energy0/D");
  data->Branch("MC_AzimuthalAngle", &azimuthalAngle,
               "MC_AzimuthalAngle/D");
  data->Branch("MC_PolarAngle", &polarAngle, "MC_PolarAngle/D");
  data->Branch("MC_XPosition", &position.at(0), "MC_XPosition/D");
  data->Branch("MC_YPosition", &position.at(1), "MC_YPosition/D");
  data->Branch("MC_ZPosition", &position.at(2), "MC_ZPosition/D");
  data->Branch("N_Groups", &activatedGroups, "N_Groups/D");
  data->Branch("N_Sensors_per_Group", &activatedSensorsPerGroup);
  data->Branch("N_Sensors", &activatedSensors, "N_Sensors/D");
  data->Branch("Q_total", &totalIntensity, "TotalIntensity/D");
  data->Branch("R_AzimuthalAngle", &reconstructedAzimuthalAngle,
               "R_AzimuthalAngle/D");
  data->Branch("R_PolarAngle", &reconstructedPolarAngle, "R_PolarAngle/D");
}


int TSimulation::GetAmountOfSamples() const {
  return amountOfSamples_;
}


void TSimulation::Simulate() {
  TSource *source = new TInIce(detectorDimensions_,
                               energyExponent_,
                               energyMin_,
                               energyMax_);
  Simulate(source);
  delete source;
}

void TSimulation::Simulate(TSource *source) {
  std::vector<TEvent *> events = source->GetNEvents(amountOfSamples_);

  for (int i = 0; i < amountOfSamples_; i++) {
    TEvent *event = events.at(i);
    eventId++;

    // create temporary variables used to store measurements
    bool measured = false;
    // TODO: hier ne geschicktere Alternative finden?
    double minArrivalTime = 100;
    double maxArrivalTime = 0;
    TSensor *firstSensor, *lastSensor;
    // clear the used variables so they have a reasonable value if
    // nothing is detected
    firstSensorX = NAN;
    firstSensorY = NAN;
    firstSensorZ = NAN;
    centerOfIntensityX = NAN;
    centerOfIntensityY = NAN;
    centerOfIntensityZ = NAN;
    effectiveTrackLength = NAN;
    activatedGroups = 0;
    activatedSensorsPerGroup.assign(detector_->GetNumberOfGroups(), NAN);
    activatedSensors = 0;
    totalIntensity = 0;
    reconstructedAzimuthalAngle = NAN;
    reconstructedPolarAngle = NAN;

    // save the information that can read from the event
    energy = event->GetEnergy();
    azimuthalAngle = event->GetAzimuthalAngle();
    polarAngle = event->GetPolarAngle();
    position = event->GetPosition();

    // detect the given event -> sensors contain measured intensity
    detector_->Detect(*event, false);

    // read data from sensors and get various features
    std::vector<TSensor *> sensors = detector_->GetSensors();
    for (uint j = 0; j < detector_->GetNumberOfSensors(); j++) {
      TSensor *sensor = sensors.at(j);

      // get intensity of the addressed sensor
      double intensity = sensor->GetIntensity();
      // store arrival time of addressed sensor
      double tempArrivalTime = sensor->GetArrivalTime();

      // if the event wasn't detectet (negative intesity or an arrival time
      // zero), skip this event
      if (intensity <= 0 || tempArrivalTime == 0) {
        continue;
      }

      measured = true;
      activatedSensors++;

      // compare the arrival time with the current minimal arrival time
      if (tempArrivalTime < minArrivalTime) {
        // replace minArrivalTime with current
        minArrivalTime = tempArrivalTime;
        firstSensor = sensor;
      }
      if (tempArrivalTime > maxArrivalTime) {
        // replace maxArrivalTime with current
        maxArrivalTime = tempArrivalTime;
        lastSensor = sensor;
      }

      // increase number of activated sensor counter for this group
      activatedSensorsPerGroup.at(sensor->GetGroup() - 1) += 1;

      // collect the total deposited intensity
      totalIntensity += intensity;
    }

    // only if the event was detected gather all the information
    if (measured) {
      measuredSamples_++;

      // calculate the center of intensity for this event
      vec_t centerOfIntensity = detector_->GetCenterOfIntensity();
      centerOfIntensityX = centerOfIntensity.at(0);
      centerOfIntensityY = centerOfIntensity.at(1);
      centerOfIntensityZ = centerOfIntensity.at(2);

      // store the position of the first sensor
      vec_t firstSensorPos = firstSensor->GetPosition();
      firstSensorX = firstSensorPos.at(0);
      firstSensorY = firstSensorPos.at(1);
      firstSensorZ = firstSensorPos.at(2);

      // collect data about number of activated groups
      for (uint j = 0; j < activatedSensorsPerGroup.size(); j++) {
        if (activatedSensorsPerGroup.at(j) > 0) {
          activatedGroups += 1;
        }
      }

      TEvent reconstructedTrack = detector_->ReconstructTrack();
      reconstructedAzimuthalAngle = reconstructedTrack.GetAzimuthalAngle();
      reconstructedPolarAngle = reconstructedTrack.GetPolarAngle();

      // first and last sensor position projected on track
      // TODO: was tun, wenn nur exakt ein Sensor was gefangen hat?
      vec_t firstPosition, lastPosition;
      firstPosition = reconstructedTrack.GetPerpFoot(
                        firstSensor->GetPosition());
      lastPosition = reconstructedTrack.GetPerpFoot(lastSensor->GetPosition());
      effectiveTrackLength = padif::distance(firstPosition, lastPosition);
    }

    // add features for this event to the root tree
    data->Fill();
  }

  data->Write();
  rootFile->Close();
  // --- end: measurement for all samples ---

  std::cout << "Number of created events: " << amountOfSamples_ << std::endl;
  std::cout << "Number of measured events: " << measuredSamples_ << std::endl;
}


TSimulation::~TSimulation() {
  // delete data;
  delete rootFile;
  delete detector_;
}
