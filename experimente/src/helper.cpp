/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#include <boost/math/constants/constants.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/uniform_on_sphere.hpp>
#include "./helper.h"

// load pi from boosts constants with double precision
double PI = boost::math::constants::pi<double>();


double padif::degToRad(double degree) {
  return (degree / 180.0) * PI;
}

double padif::distance(const vec_t &pnt1, const vec_t &pnt2) {
  double result = 0;
  for (uint i = 0; i < pnt1.size(); i++) {
    result += pow(pnt2.at(i) - pnt1.at(i), 2);
  }
  return sqrt(result);
}

double padif::absolute(const vec_t &point) {
  double result = 0;
  for (uint i = 0; i < point.size(); i++) {
    result += pow(point.at(i), 2);
  }
  return sqrt(result);
}

void padif::dumpVector(std::string phrase, const vec_t &position) {
  std::cout << phrase << " (" << position.at(0) << ", "
            << position.at(1) << ", "
            << position.at(2) << ")" << std::endl;
}

vec_t padif::sphericalToCartesian(double r, double azimuthal, double polar) {
  vec_t result(3);
  result.at(0) = r * cos(azimuthal) * sin(polar);
  result.at(1) = r * sin(azimuthal) * sin(polar);
  result.at(2) = r * cos(polar);

  return result;
}

double padif::innerProduct(const vec_t &aFactor, const vec_t &bFactor) {
  double result = 0;
  // check whether both vectors have the same dimension
  if (aFactor.size() != bFactor.size()) {
    std::cout << "padif::innerProduct():"
              << "Factors differ in dimension." << std::endl;
  }

  for (unsigned int i = 0; i < aFactor.size(); ++i) {
    // calculate the scalar product
    result += aFactor.at(i) * bFactor.at(i);
  }

  return result;
}


void padif::setSeed(int seed) {
  padif::rng_.seed(seed);
}

double padif::getUni() {
  return getUni(0.0, 1.0);
}

double padif::getUni(double min, double max) {
  boost::random::uniform_real_distribution<> uniDist(min, max);
  return uniDist(padif::rng_);
}

vec_t padif::getUniSphere() {
  return getUniSphere(3);
}

vec_t padif::getUniSphere(uint dimensions) {
  boost::random::uniform_on_sphere<> onSphereDist(dimensions);
  return onSphereDist(padif::rng_);
}

double padif::getCubed() {
  return pow(getUni(), 1.0 / 3);
}
