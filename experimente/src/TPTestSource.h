/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */


#ifndef TPTESTSOURCE_H
#define TPTESTSOURCE_H

#include "./helper.h"
#include "./TInIce.h"


class TPTestSource : public TInIce {
 public:
  /**
   * @brief Constructor setting all variables to reasonable default values
   */
  TPTestSource();

  /**
   * @brief Constructor setting all variables to the values given
   */
  TPTestSource(vec_t detectorDimensions, double energyExponent,
         double energyMin, double energyMax);

  /**
   * @brief GetNewEvent creates an event on a sphere outside the detector.
   * Starting postion and direction are random and determinded by the
   * GetNewTrack method. The energy is determined by the GetNewEnergy
   * method
   * @return a pointer to a new event
   */
  TEvent &GetNewEvent();

 protected:
};



#endif  // TPTESTSOURCE_H
