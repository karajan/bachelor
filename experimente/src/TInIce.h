/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#ifndef TINICE_H
#define TINICE_H

#include "./helper.h"
#include "./TSource.h"


class TInIce : public TSource {
 public:
  /**
   * @brief Constructor setting all variables to reasonable default values
   */
  TInIce();

  /**
   * @brief Constructor setting all variables to the values given
   */
  TInIce(vec_t detectorDimensions, double energyExponent,
         double energyMin, double energyMax);

  /**
   * @brief GetNewEvent creates an event on a sphere outside the detector.
   * Starting postion and direction are random and determinded by the
   * GetNewTrack method. The energy is determined by the GetNewEnergy
   * method
   * @return a pointer to a new event
   */
  TEvent &GetNewEvent();

 protected:
  // distributions:
  double energyExponent_;  // negative exponent of the power function
  double energyMin_;  // Minimum of the generated energy spectrum in GeV
  double energyMax_;  // Maximum of the generated energy spectrum in GeV
  // // factor of pi for azimuthal spectrum of starting positions
  // double azimuthalSpectrum_;
  // // factor of pi for polar spectrum of starting positions
  // double polarSpectrum_;

  /**
   * @brief GetNewEnergy calculates an energy value that belongs to an exponential
   * distribution with the negative exponent -energyExponent_. The distribution
   * starts at energyMin and has an open ending.
   * @return a randomly generated energy value in the given energy spectrum
   */
  double GetNewEnergy();
};



#endif  // TINICE_H

