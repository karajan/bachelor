/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#ifndef TSOURCE_H
#define TSOURCE_H

#include "./helper.h"
#include "./TEvent.h"
// #include "./Trng.h"


class TSource {
 public:
  /**
   * @brief Constructor setting all variables to reasonable default values
   */
  TSource();

  /**
   * @brief Constructor setting all variables to the values given
   */
  explicit TSource(vec_t detectorDimensions);

  /**
   * @brief GetNewEvent creates a new TEvent with random position
   * (on a plane of the detector), direction (pointing inside the detector
   * based on azimuthalSpectrum_ and polarSpectrum_) and energy (based on
   * GetNewEnergy)
   * @return a new Event
   */
  virtual TEvent &GetNewEvent() = 0;

  /**
   * @brief GetNEvents creates an array with n new Events, by default with
   * the GetNewEvent method
   * @param n is the number of Events to be created
   * @return an array of n new Events
   */
  virtual std::vector<TEvent *> GetNEvents(uint n);

  /**
   * @brief GetNewTrack creates a track (a position and direction) from a
   * starting point to a ranom point inside a sphere around the detector
   * with a radius of the detectors diameter
   * @start is the starting point of the particle
   * @return a vector with the track. The first three elements are the new
   * starting position of the track, placed on the sphere. The last two
   * elements are the direction in spherical coordinates
   */
  vec_t GetNewTrack(vec_t start);

  virtual ~TSource();

 protected:
  vec_t detectorDimensions_;
};



#endif  // TSOURCE_H
