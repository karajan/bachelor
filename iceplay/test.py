#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from root_numpy import root2array


branches = ["MC_Energy0", "Q_total"]

data1 = root2array("./build/te5e4.root", branches=branches)
e1 = data1["MC_Energy0"]
i1 = data1["Q_total"]
data2 = root2array("./build/tpem5e4.root", branches=branches)
e2 = data2["MC_Energy0"]
i2 = data2["Q_total"]

places1 = (e1 < 20000)
places2 = (e2 < 20000)

e1 = e1[places1]
e2 = e2[places2]
i1 = i1[places1]
i2 = i2[places2]

e1 = e1[i1 == 0]
e2 = e2[i2 == 0]

delta_e = e1 - e2

plt.hist(delta_e, 100)
# plt.hist(, 100)


# plt.gca().set_aspect('equal', adjustable='box')
# plt.savefig("point.pdf", bbox_inches="tight")
plt.show()
