#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from root_numpy import root2array

branches = ["MC_Energy0", "Q_total", "MC_XPosition",
            "MC_YPosition", "MC_ZPosition"]

data = root2array("./build/te5e4.root", branches=branches)
e = data["MC_Energy0"]
q = data["Q_total"]
x = data["MC_XPosition"]
y = data["MC_YPosition"]
z = data["MC_ZPosition"]


fig = plt.figure()
ax = fig.add_subplot(1,1,1)
# ax.set_yscale("log")
# ax.set_xscale("log")

ax.hist(x, y, q)
plt.show()
