/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */


#ifndef TEVENTFACTORY_H
#define TEVENTFACTORY_H

#include "./helper.h"
#include "./TEvent.h"


class TEventFactory {
 public:
  TEventFactory();

  virtual TEvent *createEvent() const;

  virtual ~TEventFactory();
};


#endif  // TEVENTFACTORY_H
