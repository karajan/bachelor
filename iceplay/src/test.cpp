

// google profiling
#include <google/profiler.h>

#include <iostream>
#include <fstream>
#include <vector>
// PADIF includes
#include "./TSimulation.h"
#include "./TSource.h"


int main() {
  ProfilerStart("");

  TSimulation *simu = new TSimulation("../config.ini");

  // TPTestSource *source = new TPTestSource();
  // simu->Simulate(source);

  simu->Simulate();


  // std::ofstream datei("../data.txt");

  // for (int i = 0; i < 3000; i++) {
  //   tii->GetNewEvent();
  // }

  // datei.close();


  ProfilerFlush();
  ProfilerStop();


  delete simu;

  return 0;
}
