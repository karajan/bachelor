/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#ifndef TPEASYMUON_H
#define TPEASYMUON_H

#include "PROPOSAL/Propagator.h"
#include "./TEvent.h"
#include "./helper.h"


class TPEasyMuon : public TEvent {
 public:
  /**
   * @brief TEvent creates a new event with a zero energy, zero angles and
   * a position that is placed at the origin of the coordinate (and detector)
   * system
   */
  TPEasyMuon();

  /**
   * @brief TEvent creates a new event with a starting energy, a given
   * position and direction (angle) for 2 dimensional calculations
   * @param energy contains the starting energy
   * @param position contains the coordiantes of the starting point
   * @param azimuthalAngle containts the aizumathal angle in relation to the
   * coordinates origin
   */
  TPEasyMuon(double energy, const vec_t &position, double azimuthalAngle);

  /**
   * @brief TEvent creates a new event with a starting energy, a given
   * position and direction (angles) for 3 dimensional calculations
   * @param energy contains the starting energy
   * @param position contains the coordiantes of the starting point
   * @param azimuthalAngle containts the aizumathal angle in relation to the
   * coordinates origin
   * @param polarAngle containts the polar angle in relation to the
   * coordinates origin
   */
  TPEasyMuon(double energy, const vec_t &position, double azimuthalAngle,
         double polarAngle);

  /**
   * @brief GetEnergy calculates the remaining energy the event has at a given
   * position
   * @param referencePosition contains the coordinates for energy calculations
   * @return energy the event has left after propagating to the given position
   */
  double GetEnergy(const vec_t &referencePosition) const;

  static void initPropagator();

  /**
   * @brief ~TPEasyMuon destroys the event object
   */
  ~TPEasyMuon();

 protected:
  static Propagator *prop;
};


#endif  // TPEASYMUON_H
