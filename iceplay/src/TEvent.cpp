/**
 *  Author: Philipp Schlunder
 *  Licence: GPL v3
 */

#include "./TEvent.h"

TEvent::TEvent() : energy_(0), azimuthalAngle_(0), polarAngle_(0) {
  position_.assign(3, 0);
}

TEvent::TEvent(double energy, const vec_t &position,
               double azimuthalAngle)
  : energy_(energy),
    position_(position),
    azimuthalAngle_(azimuthalAngle),
    polarAngle_(0) {
}

TEvent::TEvent(double energy, const vec_t &position,
               double azimuthalAngle, double polarAngle)
  : energy_(energy),
    position_(position),
    azimuthalAngle_(azimuthalAngle),
    polarAngle_(polarAngle) {
}

void TEvent::Dump() const {
  std::cout << "TEvent:" << std::endl;
  std::cout << "Starting energy / GeV: " << energy_ << std::endl;
  std::cout << "Azimuthal and polar angle /rad: " << azimuthalAngle_
            << ", " << polarAngle_ << std::endl;
  std::cout << "Starting position (x, y, z / m): ("
            << position_.at(0) << ", "
            << position_.at(1) << ", "
            << position_.at(2) << ")" << std::endl;
}

double TEvent::GetAzimuthalAngle() const {
  return azimuthalAngle_;
}

double TEvent::GetEnergy() const {
  return energy_;
}

double TEvent::GetEnergy(const vec_t &referencePosition) const {
  // travelled distance of the event
  double distance = padif::distance(position_, referencePosition);
  // --- constant energy loss ---
  double remainingEnergy, energyLoss;
  /**
   * approximated energy loss per metre from
   * "PROPOSAL for muon propagation" by Koehne et al.
   */
  energyLoss = 0.259 + 3.64e-4 * energy_;
  // using linear energy loss
  remainingEnergy = energy_ - energyLoss * distance;
  // ----------------------------
  return remainingEnergy;
}

double TEvent::GetPolarAngle() const {
  return polarAngle_;
}

const vec_t &TEvent::GetPosition() const {
  return position_;
}

vec_t TEvent::GetPerpFoot(const vec_t &referencePosition) const {
  /**
   * create dropped perpendicular foot (perpFoot) first
   * g: S = position_ + t * eventDirection
   * t = <referencePosition - position_, eventDirection>
   *     / <eventDirection, eventDirection>
   */
  vec_t eventDirection(3), difference(3), perpFoot(4);
  // create directional vector out of angles
  eventDirection = padif::sphericalToCartesian(1.0, azimuthalAngle_,
                   polarAngle_);
  // create difference vector between sensorPosition and this event
  for (int i = 0; i < 3; ++i) {
    difference.at(i) = referencePosition.at(i) - position_.at(i);
  }
  // calculate gradient of straight line
  double t = padif::innerProduct(difference, eventDirection)
             / padif::innerProduct(eventDirection, eventDirection);
  // calculate dropped perpendicular foot using the straight line equation g
  for (int i = 0; i < 3; ++i) {
    perpFoot.at(i) = position_.at(i) + t * eventDirection.at(i);
  }

  perpFoot.at(3) = t;

  return perpFoot;
}

void TEvent::SetAzimuthalAngle(double azimuthalAngle) {
  azimuthalAngle_ = azimuthalAngle;
}

void TEvent::SetEnergy(double energy) {
  energy_ = energy;
}

void TEvent::SetPolarAngle(double polarAngle) {
  polarAngle_ = polarAngle;
}

void TEvent::SetPosition(vec_t position) {
  for (unsigned int i = 0; i < 3; i++) {
    position_.at(i) = position.at(i);
  }
}


TEvent::~TEvent() { }
