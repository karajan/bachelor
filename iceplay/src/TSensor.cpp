/**
 *  Author: Philipp Schlunder
 *  Licence: GPL v3
 */

#include <vector>

#include "./TSensor.h"

TSensor::TSensor()
  : arrivalTime_(0),
    permeability_(1),
    intensity_(0),
    group_(1) {
  position_.assign(3, 0);
}

TSensor::TSensor(vec_t position)
  : position_(position),
    arrivalTime_(0),
    permeability_(1),
    intensity_(0),
    group_(1) {
}

TSensor::TSensor(vec_t position, double permeability, unsigned int group)
  : position_(position),
    arrivalTime_(0),
    permeability_(permeability),
    intensity_(0),
    group_(group) {
  // make sure permeability doesn't exceed 1.
  if (permeability > 1) {
    permeability_ = 1;
    std::cout << "The permeability should not exceed 1." << std::endl;
    std::cout << "Resetted permeability to 1." << std::endl;
  }
}

void TSensor::Dump() const {
  std::cout << "TSensor:" << std::endl;
  std::cout << "Position (x, y, z / m): ("
            << position_.at(0) << ", "
            << position_.at(1) << ", "
            << position_.at(2) << ")" << std::endl;
  std::cout << "Permeability: " << permeability_ << std::endl;
  std::cout << "Group: " << group_ << std::endl;
  std::cout << "ArrivalTime / s: " << arrivalTime_ << std::endl;
  std::cout << "Intensity / GeV / m^2: " << intensity_ << std::endl;
  std::cout << "---" << std::endl;
}

double TSensor::GetArrivalTime() const {
  return arrivalTime_;
}

unsigned int TSensor::GetGroup() const {
  return group_;
}

double TSensor::GetIntensity() const {
  return intensity_;
}

double TSensor::GetPermeability() const {
  return permeability_;
}

const vec_t &TSensor::GetPosition() const {
  return position_;
}

void TSensor::SetArrivalTime(double arrivalTime) {
  if (arrivalTime >= 0) {
    arrivalTime_ = arrivalTime;
  } else {
    arrivalTime_ = 0;
    std::cout << "ArrivalTime should be positive." << std::endl;
    std::cout << "Resetting arrival time to 0." << std::endl;
  }
}

void TSensor::SetGroup(unsigned int group) {
  group_ = group;
}

void TSensor::SetIntensity(double intensity) {
  // if intensity is negativ, the sensor is to far away to measure something
  // so set the sensor value to 0.
  if (intensity < 0) {
    intensity_ = 0;
  } else {
    intensity_ = intensity;
  }
}

void TSensor::SetPermeability(double permeability) {
  // make sure permeability doesn't exceed 1.
  if (permeability > 1) {
    permeability_ = 1;
    std::cout << "The permeability should not exceed 1." << std::endl;
    std::cout << "Resettet permeability to 1." << std::endl;
  } else {
    permeability_ = permeability;
  }
}

double TSensor::GetAcceptance(double energy) const {
  // (1 - exp( -E / 2)) ^ 3
  return pow((1 - exp(- energy / 2.0)), 3);
}

void TSensor::Detect(double energy, double arrivalTime, bool add) {
  // if the intensity isn't supposed to be added, set it 0
  if (!add) {
    SetIntensity(0);
    SetArrivalTime(0);
  }

  // trigger sensor based on energy dependent acceptance
  // if energy is < 0, it can't be detected; hopefully this check will
  // save some time
  if (energy > 0 && GetAcceptance(energy) > padif::getUni()) {
    SetArrivalTime(arrivalTime);
    // calculate deposited energy
    SetIntensity(energy * permeability_);
  }
}

void TSensor::Detect(double energy, double startTime, bool add,
                     const vec_t &startPosition) {
  double distance = padif::distance(startPosition, position_);
  // already passed time + time needed to reach the sensor from startPosition
  double arrivalTime = distance / padif::speedOfLight + startTime;
  // calculate the remaining energy
  double newEnergy = energy / (1 + pow(distance, 2));

  Detect(newEnergy, arrivalTime, add);
}


TSensor::~TSensor() { }
