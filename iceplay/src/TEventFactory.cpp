/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#include "./TEventFactory.h"

TEventFactory::TEventFactory() { }

TEvent *TEventFactory::createEvent() const {
  return new TEvent();
}

TEventFactory::~TEventFactory() { }
