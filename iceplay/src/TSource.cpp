/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/uniform_on_sphere.hpp>
#include <iostream>
#include <vector>

#include "./TSource.h"


TSource::TSource() {
  detectorDimensions_.resize(3);
  detectorDimensions_.at(0) = 1000;
  detectorDimensions_.at(1) = 1000;
  detectorDimensions_.at(2) = 1000;

  factory_ = new TEventFactory();
}

TSource::TSource(vec_t detectorDimensions, TEventFactory *factory) {
  detectorDimensions_.resize(3);
  detectorDimensions_.at(0) = detectorDimensions.at(0);
  detectorDimensions_.at(1) = detectorDimensions.at(1);
  detectorDimensions_.at(2) = detectorDimensions.at(2);

  if (detectorDimensions_.at(0) <= 0) {
    std::cout << "Your first detector dimension is not bigger than zero! "
              << "It will be set to 1000.0" << std::endl;
    detectorDimensions_.at(0) = 1000.0;
  }
  if (detectorDimensions_.at(1) <= 0) {
    std::cout << "Your second detector dimension is not bigger than zero! "
              << "It will be set to 1000.0" << std::endl;
    detectorDimensions_.at(1) = 1000.0;
  }
  if (detectorDimensions_.at(2) <= 0) {
    std::cout << "Your third detector dimension is not bigger than zero! "
              << "It will be set to 1000.0" << std::endl;
    detectorDimensions_.at(2) = 1000.0;
  }

  factory_ = factory;
}


std::vector<TEvent *> TSource::GetNEvents(uint n) {
  std::vector<TEvent *> result(n);
  for (uint i = 0; i < n; i++) {
    result.at(i) = &GetNewEvent();
  }
  return result;
}

vec_t TSource::GetNewTrack(vec_t start) {
  double norm, radius = 0;
  vec_t result(5);
  // direction: Vektor zwischen start und inSphere
  // inShere: beliebiger Punkt innerhalb der Kugel
  // radius: Radius der Kugel um den Detektor
  vec_t direction(3), inSphere(3), center(3);

  // Calculate the center of the detector
  for (int i = 0; i < 3; i++) {
    center.at(i) = detectorDimensions_.at(i) / 2.0;
  }
  // Calculate the radius of the sphere around the detector
  radius = padif::absolute(detectorDimensions_) * 0.5;  // real sphere size
  radius = 2 * radius;  // make the sphere a little bigger than the detector

  // initalize rng-Distributions
  // boost::random::uniform_real_distribution<> cubedDist(0.0, 1.0);
  // boost::random::uniform_on_sphere<> onSphereDist(3);

  // find random starting point inside the sphere and move the center
  // of the sphere to the center of the detector
  vec_t onSphere = padif::getUniSphere();
  double r = padif::getCubed() * radius;
  for (int i = 0; i < 3; i++) {
    inSphere.at(i) = r * onSphere.at(i) + center.at(i);
  }

  // calculate direction of the event
  for (int i = 0; i < 3; i++) {
    direction.at(i) = inSphere.at(i) - start.at(i);
  }

  // calculate azimuthal and polar angle from cartesian direction vector
  norm = padif::absolute(direction);
  result.at(3) = atan2(direction.at(1), direction.at(0));
  result.at(4) = acos(direction.at(2) / norm);

  // Check if the starting position is inside the spehere, if not return
  // start as the starting point of the track
  if (padif::distance(center, start) < radius) {
    result.at(0) = start.at(0);
    result.at(1) = start.at(1);
    result.at(2) = start.at(2);
    return result;
  }

  // to calculate the crossing point of the line between start and inSphere
  // and the sphere solve (x - c)^2 = r^2 and x = s + td for t
  // s = start
  // d = direction
  // c = center
  // r = radius
  double dsq = pow(padif::absolute(direction), 2);
  double p = 0;
  for (int i = 0; i < 3; i++) {
    p += 2.0 * (start.at(i) - center.at(i)) * direction.at(i) / dsq;
  }

  double q = 0;
  for (int i = 0; i < 3; i++) {
    q += (pow((start.at(i) - center.at(i)), 2)) / dsq;
  }
  q -= pow(radius, 2) / dsq;

  // the line doesnt hit the sphere
  if (pow(p / 2, 2) < q) {
    return vec_t(0);
  }

  // because start is outside the sphere and direction is pointing inside
  // the sphere, t is always positive and the smaller t value is the one we
  // are searching for
  double t = -p / 2 - sqrt(pow(p / 2, 2) - q);

  for (int i = 0; i < 3; i++) {
    result.at(i) = start.at(i) + t * direction.at(i);
  }

  return result;
}


void TSource::setFactory(TEventFactory *factory) {
  factory_ = factory;
}


TSource::~TSource() {}



  /**
   * @brief GetCrossPlane returns the intersections points between a given
   * track and a given plane.
   * @param centralPoint defines the centraly positioned point of a plane
   * @param edgeLength awaits the dimensions of the plane, use 0 for its
   * thickness
   * @param start starting position of the track
   * @param direction direction of the track
   * @return a 4 dimensional vector. 1st dimension is the distance (plane,
   * start), where as the other 3 dimensions are the 3 cartesian coordiantes
   * of the intersection point
   */
  // vec_t GetCrossPlane(vec_t centralPoint,
  //                                   vec_t edgeLength,
  //                                   vec_t start,
  //                                   vec_t direction) const;

  // vec_t GetNewTrack(vec_t detectorDimensions,
  //                                 vec_t randoms,
  //                                 double azAngle,
  //                                 double poAngle) const;

// vec_t TSource::GetNewTrack(
//   vec_t detectorDimensions,
//   vec_t randoms,
//   double az,
//   double po) {
//   double norm = 0, radius = 0;
//   // direction: Vektor zwischen start und inDetector
//   // inDetector: beliebiger Punkt innerhalb des Detektors
//   // intersection: Schnittpunkt in der Ebene
//   // radius: Radius der Kugel um den Detektor
//   // start: beliebiger Punkt auf der Kugel
//   vec_t direction(3), inDetector(3), intersection(4),
//       start(3), temp(3);

//   // standardmäßig kein Treffer (-1,-1,-1,-1,-1)
//   vec_t result(5, -1);

//   for (int i = 0; i < 3; i++) {
//     inDetector.at(i) = randoms.at(i) * detectorDimensions.at(i);
//     start.at(i) = detectorDimensions.at(i) / 2.0;
//   }

//   radius = GetDistance(detectorDimensions);
//   radius = 0.5 * radius;

//   temp = SphericalToCartesian(radius, az, po);

//   for (int i = 0; i < 3; i++) {
//     start.at(i) += temp.at(i);
//     direction.at(i) = inDetector.at(i) - start.at(i);
//   }

//   norm = GetDistance(direction);

//   // --- start and inDetector are ready
//   // calculate azimuthal and polar angle from cartesian direction vector
//   result.at(3) = atan2(direction.at(1), direction.at(0));
//   result.at(4) = acos(direction.at(2) / norm);

//   // --- start: finding intersection point between track and detector
//   // preparing vectors with the central points and dimensions of each plane
//   std::vector<vec_t > centralPoints(6), edgeLengths(6);
//   for (int i = 0; i < 3; i++) {
//     vec_t edgeLength(3);
//     for (int j = 0; j < 3; j++) {
//       if (i == j) {
//         edgeLength.at(j) = 0;
//       } else {
//         edgeLength.at(j) = detectorDimensions.at(j);
//       }
//     }
//     edgeLengths.at(2 * i) = edgeLength;
//     edgeLengths.at(2 * i + 1) = edgeLength;
//     centralPoints.at(2 * i) = vec_t(3, 0);
//     centralPoints.at(2 * i + 1) = detectorDimensions;
//   }

//   // get intersection points and corresponding distances for each plane
//   double smallestDistance = 2 * radius;
//   for (int i = 0; i < 6; i++) {
//     intersection = GetCrossPlane(centralPoints.at(i), edgeLengths.at(i),
//                                  inDetector, direction);
//     // only store information if there is an interection within the borders
//     // of the addressed plane
//     for (int k = 0; k < 3; k++) {
//       temp.at(k) = intersection.at(k + 1);
//     }

//     if (intersection.at(0) >= 0 && intersection.at(0) <= smallestDistance) {
//       for (int j = 0; j < 3; j++) {
//         result.at(j) = intersection.at(j + 1);
//       }
//       smallestDistance = intersection.at(0);
//     }
//   }

//   // DumpVector("start", start);
//   // DumpVector("inDetector", inDetector);
//   // DumpVector("direction", direction);
//   // DumpVector("result", result);

//   return result;
// }

// vec_t TSource::GetCrossPlane(vec_t suppPoint,
//     vec_t edgeLength,
//     vec_t inDetector,
//     vec_t direction) {
//   // standardmäßig kein Treffer (-1,-1,-1,-1)
//   vec_t result(4, -1);

//   // vec_t negDirection(3);
//   for (int i = 0; i < 3; i++) {
//     direction.at(i) = -1 * direction.at(i);
//   }

//   vec_t norm(3);
//   // finde die Senkrechte auf die Ebene
//   for (int i = 0; i < 3; i++) {
//     if (edgeLength.at(i) == 0) {
//       norm.at(i) = 1;
//     } else {
//       norm.at(i) = 0;
//     }
//   }

//   // Koordinatenform:
//   double konstante = 0;
//   double lambda = 0;
//   for (int i = 0; i < 3; i++) {
//     konstante += (suppPoint.at(i) - inDetector.at(i)) * norm.at(i);
//     lambda += direction.at(i) * norm.at(i);
//   }

//   // Track parallel zu oder in der Ebene
//   if (lambda == 0) {
//     return result;
//   }

//   lambda = konstante / lambda;

//   // wäre ein Treffer entgegen der Richtung von direction
//   if (lambda < 0) {
//     return result;
//   }

//   result.at(0) = 0;

//   for (int i = 0; i < 3; i++) {
//     double temp = inDetector.at(i) + lambda * direction.at(i);
//     result.at(i + 1) = temp;
//     result.at(0) += pow(temp - inDetector.at(i), 2);
//   }
//   result.at(0) = sqrt(result.at(0));

//   return result;
// }
