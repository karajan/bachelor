/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */


#ifndef TPEASYMUONFACTORY_H
#define TPEASYMUONFACTORY_H

#include "./helper.h"
#include "./TEventFactory.h"
#include "./TPEasyMuon.h"


class TPEasyMuonFactory : public TEventFactory {
 public:
  TPEasyMuonFactory();

  virtual TPEasyMuon *createEvent() const;

  ~TPEasyMuonFactory();
};


#endif  // TPEASYMUONFACTORY_H
