/**
 *  Author: Philipp Schlunder, Jens Adam
 *  Licence: GPL v3
 */


// boost includes
#include <boost/math/constants/constants.hpp>
#include <boost/program_options.hpp>
#include <boost/random.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/exponential_distribution.hpp>
#include <boost/random/normal_distribution.hpp>
// ROOT includes
#include <TCanvas.h>
#include <TFile.h>
#include <TGraph2D.h>
#include <TH1.h>
#include <TH3.h>
#include <TLegend.h>
#include <TPolyMarker3D.h>
#include <TRandom3.h>
#include <TStyle.h>
#include <TTree.h>
// google profiling
#include <google/profiler.h>
// system includes
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
// toy mc includes
#include "./TDetector.h"
#include "./TEvent.h"
#include "./TSimulation.h"


namespace po = boost::program_options;



/**
 * @brief LoadConfig reads the parameters of a given config file and sets the
 * gloabl variables
 * @desc_ holds the assignment between variables and option names
 * @vm_ is a map that stores the read options
 */
void LoadConfig(po::options_description &desc_, po::variables_map &vm_);


/**
 * @brief dumpVector creates a well readable output of the given vector
 * @param phrase awaits a string describing the use of this vector
 * @param position awaits a 3 dimensional vector
 */
void dumpVector(std::string phrase, std::vector<double> position) {
  std::cout << phrase << " (" << position.at(0) << ", "
            << position.at(1) << ", "
            << position.at(2) << ")" << std::endl;
}


int main() {
  ProfilerStart("");
  // set global style for root
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(1);
  // --- start: initializing parameters and setting default values ---
  // detector:
  double edgeLength_ = 1100;
  std::vector<double> detectorDimensions_(3);
  detectorDimensions_.at(0) = 1100;
  detectorDimensions_.at(1) = 1100;
  detectorDimensions_.at(2) = 700;
  int numberOfSensors_ = 27;
  std::string output_ = "result";

  // distributions:
  int seed_ = 0;
  double energyExponent_ = 2.7;  // negative exponent of the power function
  double energyMin_ = 1e1;  // Minimum of the generated energy spectrum in GeV
  double energyMax_ = 1e5;  // Maximum of the generated energy spectrum in GeV
  // factor of pi for azimuthal spectrum of starting positions
  double azimuthalSpectrum_ = 2;
  // factor of pi for polar spectrum of starting positions
  double polarSpectrum_ = 0;

  int amountOfSamples_ = 1e4;
  int measuredSamples_ = 0;

  TDetector *detector_;  // addresses the used detector
  // can store all created events
  std::vector<TEvent *> samples_(amountOfSamples_);
  // --- end: initializing parameters and setting default values ---

  // --- start: assigning and loading values out of config ---
  po::options_description desc_("parameters");
  desc_.add_options()
  ("settings.seed", po::value<int>(&seed_),
   "seed for the random number generators")
  ("settings.edgeLength", po::value<double>(&edgeLength_),
   "edge length of the detector")
  ("settings.numberOfSensors", po::value<int>(&numberOfSensors_),
   "number of sensors the detector should hold")
  ("settings.amountOfSamples", po::value<int>(&amountOfSamples_),
   "amount of samples that should be created")
  ("energy.exponent", po::value<double>(&energyExponent_),
   "negative value of exponent is used as exponent for energy distribution")
  ("energy.min", po::value<double>(&energyMin_),
   "minimum of the energy spectrum")
  ("energy.max", po::value<double>(&energyMax_),
   "maximum of the energy spectrum")
  ("origin.azimuthal", po::value<double>(&azimuthalSpectrum_),
   "factor of pi for azimuthal angle")
  ("origin.polar", po::value<double>(&polarSpectrum_),
   "factor of pi for polar angle")
  ("settings.output", po::value<std::string>(&output_), "output filename");

  // create map that will store the loaded values
  po::variables_map vm_;
  // load the assigned parameters of the config file into the map vm_
  LoadConfig(desc_, vm_);
  if ((azimuthalSpectrum_ < 0) || (azimuthalSpectrum_ > 2)) {
    azimuthalSpectrum_ = 2;
    std::cout << "Origin.azimuthal has to be in range [0,2]." << std::endl;
    std::cout << "Set it back to 2." << std::endl;
  }
  if ((polarSpectrum_ <= 0) || (polarSpectrum_ > 1)) {
    polarSpectrum_ = 1;
    std::cout << "Origin.polar has to be in range [0,1]." << std::endl;
    std::cout << "Set it back to 0." << std::endl;
  }
  // --- end: assigning and loading values out of config ---
  // --- start: creating and ploting detector ---
  // create individual detector from detector_config if edgeLength and
  // numberOfSensors is 0
  if (edgeLength_ == 0 && numberOfSensors_ == 0) {
    detector_ = new TDetector("../detector_config.txt");
    edgeLength_ = detector_->GetEdgeLength();
    numberOfSensors_ = detector_->GetNumberOfSensors();
    std::cout << "Created detector from detector_config.txt." << std::endl;
  } else {  // or just create a detector with equally distributed sensors
    detector_ = new TDetector(edgeLength_, numberOfSensors_);
    std::cout << "Created detector with an edge length of " << edgeLength_
              << " meters and " << numberOfSensors_ << " sensors." << std::endl;
  }
  // --- end: creating and ploting detector ---

  /**
    * Generating random numbers using the mersenne_twister algorithm provided
    * by boost and implemented after "Mersenne Twister: A 623-dimensionally
    * equidistributed uniform pseudo-random number generator", Makoto Matsumoto
    * and Takuji Nishimura, ACM Transactions on Modeling & Computer Simulation:
    * Special Issue on Uniform Random Number Generation, Vol. 8, No. 1, January
    * 1998, pp. 3-30.
    */
  boost::mt19937 rng(seed_);
  TRandom3 *grandom = new TRandom3(seed_);

  // --- start: initializing parameters for detection ---
  int eventId = 0;
  // starting position of event, default (0,0,0)
  std::vector<double> position(3, 0);
  double energy, azimuthalAngle, polarAngle;  // event attributes
  double randomNumber = grandom->Rndm();
  double activatedSensors, activatedGroups, totalIntensity;
  double firstSensorX, firstSensorY, firstSensorZ;
  double centerOfIntensityX, centerOfIntensityY, centerOfIntensityZ;
  double effectiveTrackLength;
  double reconstructedAzimuthalAngle, reconstructedPolarAngle;
  // stores activated sensors per id
  std::vector<unsigned int> activatedSensorsPerGroup;

  // add file ending to output filename
  output_ += ".root";
  TFile *rootFile = new TFile(output_.c_str(), "RECREATE");
  TTree *data = new TTree("run", "raw data of an event");

  // assign placeholders with branch attributes
  data->Branch("1stSensor_XPosition", &firstSensorX,
               "1stSensor_XPosition/D");
  data->Branch("1stSensor_YPosition", &firstSensorY,
               "1stSensor_YPosition/D");
  data->Branch("1stSensor_ZPosition", &firstSensorZ,
               "1stSensor_ZPosition/D");
  data->Branch("CoI_XPosition", &centerOfIntensityX, "CoI_XPosition/D");
  data->Branch("CoI_YPosition", &centerOfIntensityY, "CoI_YPosition/D");
  data->Branch("CoI_ZPosition", &centerOfIntensityZ, "CoI_ZPosition/D");
  data->Branch("Event_ID", &eventId, "Event_ID/Is");
  data->Branch("Event_Seed", &seed_, "Event_Seed/I");
  data->Branch("Effective_Track_Length", &effectiveTrackLength,
               "Effective_Track_Length/D");
  data->Branch("MC_Energy0", &energy, "MC_Energy0/D");
  data->Branch("MC_AzimuthalAngle", &azimuthalAngle,
               "MC_AzimuthalAngle/D");
  data->Branch("MC_PolarAngle", &polarAngle, "MC_PolarAngle/D");
  data->Branch("MC_XPosition", &position.at(0), "MC_XPosition/D");
  data->Branch("MC_YPosition", &position.at(1), "MC_YPosition/D");
  data->Branch("MC_ZPosition", &position.at(2), "MC_ZPosition/D");
  data->Branch("N_Groups", &activatedGroups, "N_Groups/D");
  data->Branch("N_Sensors_per_Group", &activatedSensorsPerGroup);
  data->Branch("N_Sensors", &activatedSensors, "N_Sensors/D");
  data->Branch("Q_total", &totalIntensity, "TotalIntensity/D");
  data->Branch("R_AzimuthalAngle", &reconstructedAzimuthalAngle,
               "R_AzimuthalAngle/D");
  data->Branch("R_PolarAngle", &reconstructedPolarAngle, "R_PolarAngle/D");
  // --- end: initializing parameters for detection ---

  // --- start: predefining distributions ---
  double pi = boost::math::constants::pi<double>();

  boost::uniform_01<> uniDist;
  boost::variate_generator<boost::mt19937 &, boost::uniform_01<> >
  GetUni(rng, uniDist);

  boost::uniform_real<> azDist(0, azimuthalSpectrum_ * pi);
  boost::variate_generator<boost::mt19937 &, boost::uniform_real<> >
  GetAzAngle(rng, azDist);

  boost::uniform_real<> poDist(0, polarSpectrum_ * pi);
  boost::variate_generator<boost::mt19937 &, boost::uniform_real<> >
  GetPoAngle(rng, poDist);
  // --- end: predefining distributions ----

  // --- start: measurement for all samples ---
  std::cout << "Starting measurement." << std::endl;

  for (int i = 0; i < amountOfSamples_; i++) {
    // reset event features
    bool measured = false;
    double minArrivalTime = 100, maxArrivalTime = 0;
    TSensor *firstSensor, *lastSensor;

    activatedSensors = 0;
    // activatedSensorsPerGroup.resize(0);
    totalIntensity = 0;

    // increase event id
    eventId++;

    // --- start: generating distributed random numbers ---
    energy = GetNewEnergy(energyMin_, energyMax_, energyExponent_, grandom);

    std::vector<double> newPosition;
    // check if GetNewTrack could produce a track that hit the detector
    // if not, request a new one
    do {
      std::vector<double> randoms(3);
      for (int j = 0; j < 3; j++) {
        randoms.at(j) = GetUni();
      }
      double az = GetAzAngle();
      double po = GetPoAngle();
      newPosition = GetNewTrack(detectorDimensions_, randoms, az, po);
    } while (newPosition.at(0) < 0);


    azimuthalAngle = newPosition.at(3);
    polarAngle = newPosition.at(4);
    for (unsigned int j = 0; j < 3; j++) {
      position.at(j) = newPosition.at(j);
    }
    // --- end: generating distributed random numbers ---

    // create event for detection
    TEvent *event = new TEvent();
    // fill its values with just generated random numbers
    event->SetEnergy(energy);
    event->SetAzimuthalAngle(azimuthalAngle);
    event->SetPolarAngle(polarAngle);
    event->SetPosition(position);
    // and store it for further handling
    samples_.at(i) = event;

    // get new random number for detection
    randomNumber = grandom->Rndm();

    // detect the given event -> sensors contain measured intensity
    detector_->Detect(*event, randomNumber);

    // prepare activatedSensorsPerGroup
    activatedSensorsPerGroup.assign(detector_->GetNumberOfGroups(), 0);

    // --- start: generating features out of measured data ---
    // calculate the center of intensity for this event
    centerOfIntensityX = detector_->GetCenterOfIntensity().at(0);
    centerOfIntensityY = detector_->GetCenterOfIntensity().at(1);
    centerOfIntensityZ = detector_->GetCenterOfIntensity().at(2);

    // read data from sensors and get various features
    for (int j = 0; j < numberOfSensors_; j++) {
      // get intensity of the addressed sensor
      double intensity = detector_->GetSensors().at(j)->GetIntensity();
      // store arrival time of addressed sensor
      double tempArrivalTime = detector_->GetSensors().at(j)->GetArrivalTime();
      // increase the counter for activated sensors if an intensity is detected
      if (intensity > 0) {
        if (!measured) {
          measured = true;
        }
        activatedSensors++;
        // and compare it with the temporary minimal arrival time
        // a value equal to zero means, the detector did not detect
        if ((tempArrivalTime != 0) && (tempArrivalTime < minArrivalTime)) {
          // replace minArrivalTime with current
          minArrivalTime = tempArrivalTime;
          // store z position = height of current sensor
          firstSensorX = detector_->GetSensors().at(j)->GetPosition().at(0);
          firstSensorY = detector_->GetSensors().at(j)->GetPosition().at(1);
          firstSensorZ = detector_->GetSensors().at(j)->GetPosition().at(2);
          firstSensor = detector_->GetSensors().at(j);
        }
        if ((tempArrivalTime != 0) && (tempArrivalTime > maxArrivalTime)) {
          // replace maxArrivalTime with current
          maxArrivalTime = tempArrivalTime;
          lastSensor = detector_->GetSensors().at(j);
        }

        // increase number of activated sensor counter for this group
        activatedSensorsPerGroup.at(
          detector_->GetSensors().at(j)->GetGroup() - 1) += 1;
      }
      // collect the total deposited intensity
      totalIntensity += intensity;
    }

    activatedGroups = 0;
    // collect data about number of activated groups
    for (unsigned int j = 0; j < activatedSensorsPerGroup.size(); j++) {
      if (activatedSensorsPerGroup.at(j) > 0) {
        activatedGroups += 1;
      }
    }

    // --- start: generate features of reconstructed track ---
    TEvent reconstructedTrack = detector_->ReconstructTrack();
    reconstructedAzimuthalAngle = reconstructedTrack.GetAzimuthalAngle();
    reconstructedPolarAngle = reconstructedTrack.GetPolarAngle();
    // first and last sensor position projected on track
    std::vector<double> firstPosition, lastPosition;
    // ToDo: Abfangen falls keine Messung
    if (measured) {
      measuredSamples_ += 1;
      firstPosition = reconstructedTrack.GetPerpFoot(
                        firstSensor->GetPosition());
      lastPosition = reconstructedTrack.GetPerpFoot(lastSensor->GetPosition());
      effectiveTrackLength = reconstructedTrack.GetDistance(firstPosition,
                             lastPosition);
    }
    // --- end: generate features of reconstructed track ---

    // add features for this event to the root tree
    data->Fill();
  }
  ProfilerFlush();
  ProfilerStop();

  // write the tree into the root file and close it
  data->Write();
  delete data;
  rootFile->Close();
  // --- end: measurement for all samples ---

  std::cout << "Number of created events: " << samples_.size() << std::endl;
  std::cout << "Number of measured events: " << measuredSamples_ << std::endl;

  return 0;
}


void LoadConfig(po::options_description &desc_, po::variables_map &vm_) {
  // --- reading settings ---
  // open config file
  std::ifstream config_file("../config.ini");
  // clear parameter storage before reading
  vm_ = po::variables_map();
  // read parameters from file into map
  po::store(po::parse_config_file(config_file, desc_), vm_);
  config_file.close();
  po::notify(vm_);
}

