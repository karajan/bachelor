/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */

#include "./TPEasyMuonFactory.h"


TPEasyMuonFactory::TPEasyMuonFactory() {
  TPEasyMuon::initPropagator();
}

TPEasyMuon *TPEasyMuonFactory::createEvent() const {
  return new TPEasyMuon();
}

TPEasyMuonFactory::~TPEasyMuonFactory() { }
