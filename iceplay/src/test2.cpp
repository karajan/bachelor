

// google profiling
// #include <google/profiler.h>

#include <iostream>
#include <fstream>
#include <vector>
// PADIF includes
// #include "./TDetector.h"
// #include "./TEvent.h"
// #include "./TSimulation.h"
// #include "./TInIce.h"
#include "./helper.h"


int main() {
  // ProfilerStart("");
  padif::setSeed(5);

  for (int i = 0; i < 10; i++) {
    std::cout << padif::getUni() << std::endl;
  }

  // ProfilerFlush();
  // ProfilerStop();

  return 0;
}
