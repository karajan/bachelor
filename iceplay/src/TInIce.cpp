/**
 *  Author: Jens Adam
 *  Licence: GPL v3
 */


#include <iostream>

#include "./TInIce.h"


TInIce::TInIce() {
  energyExponent_ = 2.0;
  energyMin_ = 10;
  energyMax_ = 10000;
  double edgeLength = 1000.0;
  detectorDimensions_.assign(3, edgeLength);
  // azimuthalSpectrum_ = 2.0;
  // polarSpectrum_ = 1.0;
}

TInIce::TInIce(vec_t detectorDimensions, double energyExponent,
               double energyMin, double energyMax, TEventFactory *factory) {
  energyExponent_ = energyExponent;
  energyMin_ = energyMin;
  energyMax_ = energyMax;
  detectorDimensions_ = detectorDimensions;
  // azimuthalSpectrum_ = azimuthalSpectrum;
  // polarSpectrum_ = polarSpectrum;

  if (energyExponent_ <= 0) {
    std::cout << "Your energy exponent has to be positive! "
              << "It will be set to 2.0" << std::endl;
    energyExponent_ = 2.0;
  }
  if (energyMin_ < 0) {
    std::cout << "Your minimum energy is negative! "
              << "It will be set to 0.0" << std::endl;
    energyMin_ = 0.0;
  }
  if (energyMax_ < 0) {
    std::cout << "Your maximum energy is negative! "
              << "It will be set to 10.0" << std::endl;
    energyMax_ = 10000.0;
  }
  if (energyMin_ >= energyMax_) {
    std::cout << "Your maximum energy is not higher than the minimum energy! "
              << "It will be set to minimum energy + 10.0" << std::endl;
    energyMax_ = energyMin_ + 10.0;
  }
  // if (azimuthalSpectrum_ < 0.0 || azimuthalSpectrum_ > 2.0) {
  //   std::cout << "Your azimuthal spectrum is not in [0, 2]! "
  //             << "It will be set to 2.0" << std::endl;
  //   azimuthalSpectrum_ = 2.0;
  // }
  // if (polarSpectrum_ < 0 || polarSpectrum_ > 1) {
  //   std::cout << "Your polar spectrum is not in [0, 1]! "
  //             << "It will be set to 1.0" << std::endl;
  //   polarSpectrum_ = 1.0;
  // }

  factory_ = factory;
}

/**
 * generate a random number y_i that follows an exponential distribution g(x)
 * and has the probability density g(y) whereas x is a uniformly distributed
 * random number with a probability density f(x) that is 1 for 0 <= x < 1 and
 * f(x) = 0 for x < 0 or x >= 1.
 * Using g(y) = | dx / dy | * f(x) one receivs
 * x = G(y) = \int \limits_{-\infty}^{y} g(t) dt which leads to
 * y = G^(-1)(x) where G^(-1) generates exponential distributed values.
 * inspired by Prof. Dr. Dr. Wolfgang Rhodes script about "statistical methods
 * for data analysis" page 41, from 12th october 2012.
 */
double TInIce::GetNewEnergy() {
  double base, exponent;
  exponent = 1 - energyExponent_;
  base = (pow(energyMax_, exponent) - pow(energyMin_, exponent)) *
          padif::getUni() + pow(energyMin_, exponent);

  return pow(base, 1 / exponent);
}


TEvent &TInIce::GetNewEvent() {
  double radius = padif::absolute(detectorDimensions_) * 0.5;  // sphere size
  radius = 4 * radius;  // make the sphere a little bigger than the detector

  // produce a starting point to feed to the GetNewTrack-Method
  // and move the center of the sphere to the center of the detector
  vec_t newTrack(5), start(3);
  vec_t onSphere = padif::getUniSphere();
  for (int i = 0; i < 3; i++) {
    start.at(i) = onSphere.at(i) * radius + detectorDimensions_.at(i) / 2.0;
  }

  // this method is guaranteed to produce a good track so no checking
  newTrack = GetNewTrack(start);
  vec_t position(3);
  double azimuthalAngle = newTrack.at(3);
  double polarAngle = newTrack.at(4);
  for (uint j = 0; j < 3; j++) {
    position.at(j) = newTrack.at(j);
  }

  double energy = GetNewEnergy();

  TEvent *event = factory_->createEvent();
  // fill its values with just generated values
  event->SetEnergy(energy);
  event->SetAzimuthalAngle(azimuthalAngle);
  event->SetPolarAngle(polarAngle);
  event->SetPosition(position);

  return *event;
}
