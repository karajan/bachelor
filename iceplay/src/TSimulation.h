/**
 *  Author: Philipp Schlunder, Jens Adam
 *  Licence: GPL v3
 */

#ifndef TSIMULATION_H
#define TSIMULATION_H


#include <boost/program_options.hpp>

#include <TFile.h>
#include <TTree.h>

#include "./helper.h"
#include "./TDetector.h"
#include "./TSource.h"
#include "./TEventFactory.h"


namespace po = boost::program_options;


class TSimulation {
 public:
  /**
   * @brief TSimulation initalizes a new instace with standard values
   */
  TSimulation();

  /**
   * @brief TSimulation initalizes a new instace with values read from a
   * config file
   * @param configFile is the path to the configFile to be used
   */
  explicit TSimulation(std::string configFile);

  /**
   * @brief GetAmountOfSamples returns amount of samples to be used
   * @return the amount of samples
   */
  int GetAmountOfSamples() const;

  /**
   * @brief Simulate runs a simulation based on the current config and saves
   * the data to a TFile
   */
  void Simulate();

  /**
   * @brief Simulate runs a simulation with the events of the TSource given
   * and saves the data to a TFile
   * @param source is a pointer to a TSource instance
   */
  void Simulate(TSource *source);


  ~TSimulation();

 protected:
  // detector
  double edgeLength_;
  vec_t detectorDimensions_;
  int numberOfSensors_;
  std::string detectorConfig_;  // path to the file with detector config
  TDetector *detector_;  // addresses the used detector

  std::string factoryType_;
  TEventFactory *factory_;

  // po::config
  po::options_description desc_;
  po::variables_map vm_;

  // general measurement information
  int amountOfSamples_;
  int measuredSamples_;

  // root files
  std::string output_;
  TFile *rootFile;
  TTree *data;

  // event attributes needed to save to root file
  // in order of their appearance
  double firstSensorX, firstSensorY, firstSensorZ;
  double centerOfIntensityX, centerOfIntensityY, centerOfIntensityZ;
  int eventId;
  int seed_;
  double effectiveTrackLength;
  double energy, azimuthalAngle, polarAngle;
  vec_t position;  // starting position of event
  double activatedGroups;
  std::vector<uint> activatedSensorsPerGroup;  // activated sensors per id
  double activatedSensors;
  double totalIntensity;
  double reconstructedAzimuthalAngle, reconstructedPolarAngle;


  // TODO legacy!
  double energyExponent_;
  double energyMin_;
  double energyMax_;
  double azimuthalSpectrum_;
  double polarSpectrum_;


  /**
   * @brief InitConfig prepares desc_ and vm_ to read a config file
   */
  void InitConfig();

  /**
   * @brief InitTTree prepares TFile rootFile and TTree data to save the data
   */
  void InitTTree();

  /**
   * @brief LoadConfig reads the parameters of a given config file and sets the
   * gloabl variables
   * @desc_ holds the assignment between variables and option names
   * @vm_ is a map that stores the read options
   */
  void LoadConfig(std::string configFile);
};

#endif  // TSIMULATION_H
